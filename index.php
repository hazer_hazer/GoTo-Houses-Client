<?php

	session_start();
	include('db.php');
	$res = mysql_query("SET NAMES 'utf8'", $db) or die(mysql_error());



?>
<!DOCTYPE html>
<html>
<?php
	// include('db.php');
	// $res = mysql_query("SELECT * FROM users", $db) or die(mysql_error());
	// $res = mysql_fetch_array($res);
	// exit($res['login'].'   '.$res['password']);
?>
<head>
	<meta charset="utf-8">
	<title>GoTo</title>
	<style type="text/css">
		* {
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			user-select: none; 
		}
		img{
			-moz-user-select: none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			user-select: none; 
		}
		body{
			width: 100%;
			height: 100%;
			padding: 0;
			margin: 0;
			text-align: center;
		}
		#bar{
			display: none;
			position: relative;
			margin: 10px auto;
			width: 750px;
			overflow-y: auto;
			min-height: 45px;
			max-height: 100px;
			height: max-content;
			border: 1.5px solid #ddd;
			font-family: Trebuchet MS, sans-serif;
			font-size: 15px;
			color: #444444;
			border-radius: 10px;
			text-align: left;
			padding: 5px;
		}
		#bar .headline{
			display: inline-block;
			position: relative;
		}
		#bar .block{
			display: block;
			position: relative;
			height: max-content;
			width: 95%;
			padding: 2.5px 2.5%;
			text-align: left;
		}
		canvas{
			position: absolute;
			top: 125px;
			left: 225px;
			margin: 0 auto;
			background: #f0f0f0;
			border-radius: 25px;
			border: 1.5px solid #ddd;
		}
		.problem{
			display: inline-block;
			position: relative;
			margin: 0;
			line-height: 25px;
			vertical-align: middle;
			border-bottom: 1.2px solid #ddd;
		}
		.problem img{
			display: inline-block;
			position: relative;
			width: 15px;
			height: 15px;
			margin-left: 5px;
			vertical-align: text-bottom;
			border-radius: 50%;
		}
		.problem img:hover{
			background: #cd853f;
		}
		#signin_window{
			display: block;
			position: relative;
			margin: 100px auto;
			width: 250px;
			height: 150px;
			border: 1.4px solid #cd853f;
			background: #fafafa;
			font-family: Trebuchet MS, sans-serif;
			font-size: 17.5px;
			color: #444444;
			text-align: center;
			padding: 10px;
			border-radius: 10px;
		}
		#edit{
			display: block;
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			border-radius: 10px;
			padding: 15px;
			float: right;
			width: 25px;
			height: 25px;
			transition: 0.25s ease;
			cursor: pointer;
			line-height: 100%;
			vertical-align: middle;
		}
		#edit:hover{
			box-shadow: inset 5px 0 5px #ccc;
			opacity: 0.85;
		}
		#edit_menu{
			display: none;
			position: absolute;
			top: 75px;
			left: 42.5%;
			width: 250px;
			height: max-content;
			padding: 10px;
			background: #f0f0f0;
			border: 1.5px solid #ddd;
			z-index: 100000;
		}
		#gender{
			display: block;
			position: relative;
			padding: 2.5px;
			background: #f0f0f0;
		}
		#signin_window > h3{
			display: block;
			position: relative;
			color: #4C2F27;
		}
		#signin_window > input{
			display: block;
			position: relative;
			margin: 5px auto;
			padding: 2.5px 10px;
			border: 1.4px solid #ddd;
			border-radius: 5px;
			box-shadow: 0 0 10px #ccc;
			color: #4C2F27;
			outline: none;
		}
		#submit_signin{
			display: block;
			position: relative;
			width: max-content;
			margin: 10px auto;
			padding: 2.5px 10px;
			background: #fafafa;
			font-family: Trebuchet MS, sans-serif;
			font-size: 17.5px;
			color: #444444;
			cursor: pointer;
			border-radius: 10px;
			transition: 0.25s ease;
		}
		#submit_signin:hover{
			box-shadow: inset 0 5px 5px #ddd;
		}
		#submit_signin:active{
			box-shadow: inset 0 5px 5px #cd853f;
		}
		#signin_window > input:focus{
			box-shadow: 0 0 5px #cd853f;
			border: 1.4px solid #E5BE01;
		}
		header{
			display: block;
			position: absolute;
			top: 0;
			left: 0;
		}
		header > img{
			display: block;
			position: absolute;
			top: 0;
			left: 0;
			border-right: 2px solid #cd853f;
			border-bottom: 2px solid #cd853f;
			border-radius: 0 0 10px 0;
			z-index: 1000;
			box-shadow: 0 8px 10px #e5bf9a;
		}
		header > h1{
			display: block;
			position: absolute;
			top: 0;
			left: 150px;
			background: #E5BE01;
			color: #4C2F27;
			border-right: 1.5px solid #cd853f;
			border-bottom: 1.5px solid #cd853f;
			border-radius: 0 2.5px 15px 2.5px;
			margin: 0;
			padding: 7.5px;
			vertical-align: middle;
			font-family: Trebuchet MS, sans-serif;
			z-index: 0;
			box-shadow: 0 5px 5px #e5bf9a;
		}
		#logout{
			display: block;
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			background: #E5BE01;
			color: #4C2F27;
			border-left: 1.5px solid #cd853f;
			border-bottom: 1.5px solid #cd853f;
			border-radius: 0 2.5px 2.5px 15px;
			margin: 0;
			padding: 7.5px;
			vertical-align: middle;
			font-family: Trebuchet MS, sans-serif;
			transition: 0.25s ease;
			cursor: pointer;
		}
		#logout:hover{
			opacity: 0.75;
		}
		#update{
			display: block;
			position: absolute;
			width: 25px;
			height: 25px;
			border-radius: 2.5px 15px 15px 2.5px;
			padding: 25px;
			margin: 0;
			border: 1.5px solid #cd853f;
			border-left: none;
			left: 0;
			top: 45%;
			cursor: pointer;
			transition: 0.15s ease-out;
		}
		#update:hover{
			box-shadow: inset -15px 0 10px #bbb;
		}
		#update:hover > img{
			transform: rotate3d(0, 1, 0, -25deg);
		}
	</style>
	<script type="text/javascript" src="js/jquery.js"></script>
</head>
<body>
	<header>
		<img src="drawable/goto_logo.jpg" width="150px"><h1>Houses</h1>
	</header>
	<?php
		if(!$_SESSION['login'] || empty($_SESSION['login'])){
		?>
		<div id="signin_window">
			<h3>Вход</h3>
			<input type="text" id="login" placeholder="Логин">
			<input type="password" id="password" placeholder="Пароль">
			<div id="submit_signin">
				Войти
			</div>
		</div>
		<script type="text/javascript">
			$('#submit_signin').click(function(){
				var login = $('#login').val();
				var password = $('#password').val();
				$.post({
					url: 'signin.php',
					dataType: 'html',
					data: ({login: login, password: password}),
					success: function(res){
						if(res == 'init'){
							location.reload();
						}else if(res == 'pit'){
							alert('Неверный пароль');
						}else{
							alert('Ошибка');
						}
					}
				});
			});
		</script>
		<?php
		}else{
	?>
	<div id="bar">
	<!-- 	<div id="beds" class="block">
			<div class="headline">Кровати: </div>
			<content></content>
		</div> -->
		<div id="problems" class="block">
			<div class="headline">Проблемы: </div>
			<content></content>
		</div>
		<div id="edit">
			<img src="drawable/edit.svg" width="25px" height="25px">
		</div>
	</div>
	<div id="edit_menu">
		<label>Пол: </label>
		<div id="gender">m</div>
	</div>
	<div id="logout">
		Выйти
	</div>
	<div id="update">
		<img src="drawable/refresh.svg" width="25px" height="25px">
	</div>
	<canvas id="canvas" width="1250" height="575"></canvas>
	<script type="text/javascript">
		var cvs = document.getElementById('canvas');
		var ctx = cvs.getContext('2d');
		var bar = document.getElementById('bar');

		var mouseX, mouseY;

		cvs.addEventListener('click', event => {

			mouseX = event.clientX - cvs.offsetLeft;
			mouseY = event.clientY - cvs.offsetTop; 

			for(var i = 0; i < houses.length; i++){
				houses[i].hover(mouseX, mouseY) ? houses[i].on_click() : false;
			}

		});

		var id, problem;

		$('body').on('click', '.problem img', function(){
			id = $(this).parent().attr('data-id');
			problem = $(this).parent();

			$.post({
				url: 'delete_problem.php',
				dataType: 'html',
				data: ({id: id}),
				success: function(res){
					if(res) problem.remove();
					if($('#problems content .problem').length <= 0){
						$('#problems content').html('Всё в порядке');
						warning();
					}
				}
			});

		});
		$('#logout').click(function(){
			$.post({
				url: 'logout.php',
				dataType: 'html',
				data: ({truth: true}),
				success: function(res){
					if(res) location.reload();
				}
			});
		});

		var warnings = [];

		$('#update').click(function(){
			warning();
		});
		var house = function(x, y, w, h, num){
			this.x = x,
			this.y = y,
			this.w = w,
			this.h = h,
			this.c = '#cd853f',
			this.text_c = '#333333',
			this.num = num,
			this.info = false,
			this.beds = 0,

			this.drawing = function(){
				this.c = this.info ? '#E5BE01' : '#cd853f';
				for(var i = 0; i < warnings.length; i++){
					if(this.num == warnings[i]) this.c = '#ff5959';
				}
				ctx.fillStyle = this.c;
				ctx.fillRect(this.x, this.y, this.w, this.h);

				ctx.font = "30px Trebuchet MS";
				ctx.fillStyle = this.text_c;
				ctx.textAlign = "center";
				var out = this.num;
				if(this.num > 30){
					out[2] = out[1];
					out[1] = '.';
					out = out/10;
				}
				ctx.fillText(out, this.x+this.w/2, this.y+this.h/1.5);
			},

			this.hover = function(x, y){
				if(x < this.x+this.w && y < this.y+this.h && x > this.x && y > this.y){
					return true;
				}else{
					return false;
				}
			},
			this.on_click = function(){
				$('#edit_menu').slideUp(100);
				for(var i = 0; i < houses.length; i++){
					if(houses[i].num != this.num) houses[i].info = false;
				}
				this.info = !this.info;
				this.c = this.info ? '#E5BE01' : '#cd853f';
				if(this.info){
					$.post({
						url: 'get_problems.php',
						dataType: 'json',
						data: ({num: num}),
						success: function(res){
							if(res){
								bar.style.display = 'block';
								$('#problems').find('content').html(res);
							}else{
								bar.style.display = 'block';
								$('#problems').find('content').html('Всё в порядке');
							}
						}
					});
				} 
				else bar.style.display = 'none';
			},
			this.get_info = function(){
				
			}
		}

		var road = function(x1, y1, x2, y2, w){
			this.x1 = x1,
			this.y1 = y1,
			this.x2 = x2,
			this.y2 = y2,
			this.c = '#888888',
			this.w = w,

			this.drawing = function(){
				ctx.beginPath();
				ctx.strokeStyle = this.c;
				ctx.lineWidth = this.w;
				ctx.moveTo(x1, y1);
				ctx.lineTo(x2, y2);
				ctx.stroke();
			}
		}
		var roads = [];
		var houses = [];

		var left = 22;
		var right = 12;
		for(var i = 0; i < 7; i++){
			houses.push(new house(i*(right > 9 ? 150 : 100) + (right > 9 ? 150 : 265), 150, 75, 60, right));
			right--;
		}
		for(var i = 0; i < 10; i++){
			houses.push(new house(i*85+105, cvs.height-150, 75, 60, left));
			left--;
		}
		third = 4;
		for(var i = 0; i < 4; i++){
			houses.push(new house(cvs.width - 100, i*105+125, 100, 70, '3'+third.toString()));
			third--;
		}

		roads[0] = new road(cvs.width, cvs.height*0.55, cvs.width-cvs.width*0.9575, cvs.height*0.55, 40);
		roads[1] = new road(cvs.width-cvs.width*0.9575, 0, cvs.width-cvs.width*0.9575, cvs.height, 30);

		for(var i = 0; i < houses.length; i++){
			roads.push(new road(houses[i].x+houses[i].w/2,
								houses[i].y+houses[i].h/2, 
								houses[i].x+houses[i].w/2, 
								houses[i].y < cvs.height/2 ? houses[i].y+houses[i].h+100 : houses[i].y-100,
								25));
		}

		// var male;
		// $('#edit').click(function(){
		// 	$('#edit_menu').slideToggle();
		// });
		// $('#gender').click(function(){
		// 	alert('click');
		// 	$('#gender').text($('#gender') == 'm' ? 'f' : 'm');
		// });

		// for(var i = 0; i < houses.length; i++){
		// 	houses[i].init();
		// }

		draw();

		warning();

		function draw(){

			for(var i = 0; i < roads.length; i++){
				roads[i].drawing();
			}
			for(var i = 0; i < houses.length; i++){
				houses[i].drawing();
			}
			window.requestAnimationFrame(draw);
		}


		function clear_all(){
			ctx.clearRect(0, 0, cvs.width, cvs.height);
		}
		function warning(){
			$.post({
				url: 'warnings.php',
				dataType: 'json',
				data: ({truth: true}),
				success: function(res){
					warnings = res;
				}
			});
		}
		
	</script>
	<?php
	}
	?>
</body>
</html>